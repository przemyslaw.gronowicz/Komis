/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.komis.constants;
/**
 *
 * @author PsLgComp
 */
public class Car {
    public static final char STATUS_SOLD = 'S';
    public static final char STATUS_AVIABLE_FOR_SALE = 'A';
    public static final Character STATUS_NOT_SET = null;
    
    public static final char FUEL_DIESEL = 'D';
    public static final char FUEL_GASOLINE = 'G';
    public static final char FUEL_LPG = 'L';
    public static final char FUEL_LPG_AND_GASOLINE = 'P';
    public static final char FUEL_ELECTRIC = 'E';
    public static final char FUEL_HYBRID = 'H';
    
    public static final char GEAR_MANUAL = 'M';
    public static final char GEAR_AUTOMATIC = 'A';
}
