/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.komis.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author RENT
 */
@Entity
@DiscriminatorValue("B")
public class BusinessCustomer extends Customer implements Serializable {
    @Column(name = "name", length = 50)
    private String name;
    @Column (name = "nip")
    private String NIP;    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getNip() {
        return NIP;
    }

    @Override
    public void setNip(String nip) {
        NIP = nip;}
    

    
}
