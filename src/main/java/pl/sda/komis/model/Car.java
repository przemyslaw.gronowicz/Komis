/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.komis.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author PsLgComp
 */
@Entity
@Table(name = "cars")
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "vin", columnDefinition = "char(17)")
    private String vin; 
    @Column(name = "mfg_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date mfgDate;
    @Column
    private String mark;
    @Column
    private String model;
    @Column
    private Integer mileage;
    @Column(name = "insurance_no")
    private Integer insuranceNo;
    @Column (name = "registration_no")
    private String registrationNo;
    @Column(name = "reg_doc_no")
    private String RegDocNo;
    @Column//A - auto; M - manual
    private Character gear;
    @Column
    //D(-iesel); G(-asoline); L(-PG); P  = G+L; E(-lectric); (H-ybrid) 
    private Character FuelType;
    @Column (length = 1000)
    private String description;
    @Column( name = "asking_price", precision = 18 , scale = 2)
    private BigDecimal askingPrice;
    @Column (name = "engine_vol")
    private Integer engVolume;
    @Column (name = "engine_pow")
    private Integer engPower;
    @Column (name = "test_drives")
    private Integer testDrivesCnt;
    //owner (customer) jesli nill to ownerem jest komis
    //private Long customerID;
    @Column//N - not set; A - aviable for sale; S - sold;
    private Character status;
    
    
//    asking_price NUMERIC(9, 2),
//    invoice_price NUMERIC(9, 2),    
    

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getMfgDate() {
        return mfgDate;
    }

    public void setMfgDate(Date mfgDate) {
        this.mfgDate = mfgDate;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getInsuranceNo() {
        return insuranceNo;
    }

    public void setInsuranceNo(Integer insuranceNo) {
        this.insuranceNo = insuranceNo;
    }

    public String getRegDocNo() {
        return RegDocNo;
    }

    public void setRegDocNo(String RegDocNo) {
        this.RegDocNo = RegDocNo;
    }

    public Character getGear() {
        return gear;
    }

    public void setGear(Character gear) {
        this.gear = gear;
    }

    public Character getFuelType() {
        return FuelType;
    }

    public void setFuelType(Character FuelType) {
        this.FuelType = FuelType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAskingPrice() {
        return askingPrice;
    }

    public void setAskingPrice(BigDecimal askingPrice) {
        this.askingPrice = askingPrice;
    }
    
    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public Integer getEngVolume() {
        return engVolume;
    }

    public void setEngVolume(Integer engVolume) {
        this.engVolume = engVolume;
    }

    public Integer getEngPower() {
        return engPower;
    }

    public void setEngPower(Integer engPower) {
        this.engPower = engPower;
    }

    public Integer getTestDrivesCnt() {
        return testDrivesCnt;
    }

    public void setTestDrivesCnt(Integer testDrivesCnt) {
        this.testDrivesCnt = testDrivesCnt;
    }


    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
    

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vin != null ? vin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Car)) {
            return false;
        }
        Car other = (Car) object;
        if ((this.vin == null && other.vin != null) || (this.vin != null && !this.vin.equals(other.vin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.komis.model.Car[ id=" + vin + " ]";
    }
    
}
