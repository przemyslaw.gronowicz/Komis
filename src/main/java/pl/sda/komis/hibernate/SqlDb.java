/**
 *
 * @author Przemek
 *
 * Założenie: sesją zarządza hibernate -> wpis w hibernate.cfg:
 * <property name="hibernate.current_session_context_class">thread</property>
 * (sesja jest utrzymywana w kontekście wątku)
 * Takie podejście wymusza otwieranie transakcji nawet do odczytu danych.
 */
package pl.sda.komis.hibernate;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class SqlDb {

    public static Session getSession() {
        return ConfigHibernate.getInstance().getCurrentSession();
    }

    public static <T> List<T> selectAll(Class<T> type) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        Object res = session
                .createQuery("from " + type.getSimpleName()).list();
        tx.commit();
        return (List<T>) res;
    }

    public static <En> void add(En entity) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(entity);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public static <En> void delete(En entity) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        try {
            session.delete(entity);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }

        // getSession().delete(entity);
    }

    public static <E> Object getById(Class<E> type, Serializable id) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        Object obj = session.get(type, id);
        tx.commit();
        return obj;
    }

    public static <E> boolean addIfNotExists(E entity, Serializable id) {
        if (getById(entity.getClass(), id) != null) {
            return false;
        }
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(entity);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        return true;
    }

    //funkcja zwraca listę obiektów których pola o nazwie fieldName mają wartość fieldVal
    public static <T, F> List<T> selectByField(Class<T> type, String fieldName, F fieldVal) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        List<T> list;
        if (fieldVal != null) {
            list = session.createQuery(
                    //session.createQuery("select c from Car c where c.status = 'S'").list();
                    "select x from " + type.getSimpleName() + " x where x." + fieldName + " = :param")
                    .setParameter("param", fieldVal).list();
        } else {

            list = session.createQuery(
                    "select x from " + type.getSimpleName() + " x where x." + fieldName + " is null")
                    .list();
        }
        tx.commit();
        return list;
    }
//  //przykład z netu
//    List results = this.sessionFactory.getCurrentSession()
//            .createQuery("select au from AppUser au where au.email = :email")
//            .setParameter("email", email)
//            .list();
//    
//    //przykład z netu
//    //boolean result = deleteById(Product.class, new Integer(41));
//    private boolean deleteById(Class<?> type, Serializable id) {
//        Object persistentInstance = em.find(type, id);
//        if (persistentInstance != null) {
//            em.remove(persistentInstance);
//            return true;
//        }
//        return false;
//    }

}
