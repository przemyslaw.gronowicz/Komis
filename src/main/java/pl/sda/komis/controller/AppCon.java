/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.komis.controller;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.*;
/**
 *
 * @author PsLgComp
 */
@ManagedBean(name = "appCon")
@ApplicationScoped


public class AppCon {
    private String version = "0.1";
     /**
     * Creates a new instance of AppCon
     */
    
    public AppCon(){        
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
    

    
}
