/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.komis.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import static pl.sda.komis.constants.Car.*;
import pl.sda.komis.hibernate.SqlDb;
import pl.sda.komis.model.Car;

/**
 *
 * @author Przemek
 */
@ManagedBean(name = "carsCon")
@RequestScoped
public class CarsCon {

    private Car newCar = new Car();
    public String test;

    /**
     * Creates a new instance of CarsCon
     */
    public CarsCon() {
    }

    private void showMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
    
    public String getCarStatusStyle(Car car){
        Character cs = car.getStatus();
        if (cs == STATUS_NOT_SET) return "black"; //STATUS_NOT_SET = null
        if (cs.equals(STATUS_SOLD)) return "gray";
        if (cs.equals(STATUS_AVIABLE_FOR_SALE)) return "blue";
        return "red";
    }
   

    public List<Car> getCars() {
        return SqlDb.selectAll(Car.class);
    }

    public List<Car> getCarsSold(){
        return SqlDb.selectByField(Car.class,"status", STATUS_SOLD);
    }
    
    public List<Car> getCarsOther() {
        return SqlDb.selectByField(Car.class, "status", null);
    }
    
    public List<Car> getCarsOnSale(){
        return SqlDb.selectByField(Car.class, "status", STATUS_AVIABLE_FOR_SALE);
    }
    
    public void addCar() {
        if (newCar.getVin().length() < 5){ 
            showMessage("Vin powinien być dłuższy niż 5 znaków .."); 
            return;
        }
        if (!SqlDb.addIfNotExists(newCar, newCar.getVin())) {
            showMessage("Samochód z tym numerem VIN jest już w bazie");
        }
    }

    public void deleteCar(Car car) {
        SqlDb.delete(car);
    }

    public void printCar() {
        System.out.println(newCar.getMark());
        System.out.println(newCar.getModel());
        System.out.println(newCar);
    }

    public String getVin() {
        return newCar.getVin();
    }

    public void setVin(String vin) {
        newCar.setVin(vin);
    }

    public Date getMfgDate() {
        return newCar.getMfgDate();
    }

    public void setMfgDate(Date mfgDate) {
        newCar.setMfgDate(mfgDate);
    }

    public String getMark() {
        return newCar.getMark();
    }

    public void setMark(String mark) {
        newCar.setMark(mark);
    }

    public String getModel() {
        return newCar.getModel();
    }

    public void setModel(String model) {
        newCar.setModel(model);
    }

    public Integer getMileage() {
        return newCar.getMileage();
    }

    public void setMileage(Integer mileage) {
        newCar.setMileage(mileage);
    }

    public Integer getInsuranceNo() {
        return newCar.getInsuranceNo();
    }

    public void setInsuranceNo(Integer insuranceNo) {
        newCar.setInsuranceNo(insuranceNo);
    }

    public String getRegDocNo() {
        return newCar.getRegDocNo();
    }

    public void setRegDocNo(String RegDocNo) {
        newCar.setRegDocNo(RegDocNo);
    }

    public Character getGear() {
        return newCar.getGear();
    }

    public void setGear(Character gear) {
        newCar.setGear(gear);
    }

    public Character getFuelType() {
        return newCar.getFuelType();
    }

    public void setFuelType(Character FuelType) {
        newCar.setFuelType(FuelType);
    }

    public String getDescription() {
        return newCar.getDescription();
    }

    public void setDescription(String description) {
        newCar.setDescription(description);
    }

    public BigDecimal getAskingPrice() {
        return newCar.getAskingPrice();
    }

    public void setAskingPrice(BigDecimal askingPrice) {
        newCar.setAskingPrice(askingPrice);
    }

    public String getRegistrationNo() {
        return newCar.getRegistrationNo();
    }

    public void setRegistrationNo(String registrationNo) {
        newCar.setRegistrationNo(registrationNo);
    }

    public Integer getEngVolume() {
        return newCar.getEngVolume();
    }

    public void setEngVolume(Integer engVolume) {
        newCar.setEngVolume(engVolume);
    }

    public Integer getEngPower() {
        return newCar.getEngPower();
    }

    public void setEngPower(Integer engPower) {
        newCar.setEngPower(engPower);
    }

    public Integer getTestDrivesCnt() {
        return newCar.getTestDrivesCnt();
    }

    public void setTestDrivesCnt(Integer testDrivesCnt) {
        newCar.setTestDrivesCnt(testDrivesCnt);
    }

    public Character getStatus() {
        return newCar.getStatus();
    }

    public void setStatus(Character status) {
        newCar.setStatus(status);
    }

    public void generateTestCar() {
        newCar.setVin(new SimpleDateFormat("yyyyMMdd.HH.mm.ss").format(new Date()));
        newCar.setAskingPrice(new BigDecimal(5000));
        newCar.setMark("Peugeot");
        newCar.setMfgDate(new Date());
        newCar.setModel("207");
        newCar.setEngPower(70);
        newCar.setEngVolume(1400);
        newCar.setFuelType(FUEL_DIESEL);
        newCar.setGear(GEAR_MANUAL);
        newCar.setMileage(100);
        newCar.setInsuranceNo(1234346);
        newCar.setRegistrationNo("ABF26423");
        newCar.setRegDocNo("123456789");
        newCar.setDescription("prawie nowy wspaniały samochód");
        //newCar.setStatus(null);
        newCar.setTestDrivesCnt(null);
    }

}
