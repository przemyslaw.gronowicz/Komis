/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.komis.controller;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import pl.sda.komis.hibernate.SqlDb;
import pl.sda.komis.model.BusinessCustomer;
import pl.sda.komis.model.Customer;
import pl.sda.komis.model.PrivateCustomer;

/**
 *
 * @author Przemek
 */
@ManagedBean(name = "custCon")
@javax.faces.bean.RequestScoped
public class CustCon {
    private Customer newBusinessCustomer = new BusinessCustomer();
    private Customer newPrivateCustomer = new PrivateCustomer();
    private boolean fCustomerTypeBusiness = false;

    /**
     * Creates a new instance of CustCon
     */
    public CustCon() {
    }
    
    private void showMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
    
    public void customerTypeBusinessSet(boolean business){
        fCustomerTypeBusiness = business;
    }
    
    public List<Customer> getCustomers() {
        return SqlDb.selectAll(Customer.class);
    }
    
    public void deleteCustomer(Customer customer) {
        SqlDb.delete(customer);
    }
    
    private Customer getActiveCustomer(){
        if (fCustomerTypeBusiness) {return newBusinessCustomer;} else
        {return newPrivateCustomer;}
        
    }
    
//    public void addCustomer() {
//        SqlDb.add(getActiveCustomer());
//    }
    
    public void addBuisnessCustomer() {
        SqlDb.add(newBusinessCustomer);
    }
    
    public void addPrivateCustomer() {
        SqlDb.add(newPrivateCustomer);
    }
        
    public void setNip(String nip){
        newBusinessCustomer.setNip(nip);
    }
    
    public String getNip(){
        return newBusinessCustomer.getNip();
    }
    
    public void setName(String name) {
        newBusinessCustomer.setName(name);
    }
    
    public String getName(){
        return newBusinessCustomer.getName();
    }
    
    public String getAddress(){
        return getActiveCustomer().getAddress();
    }
    
    public void setAddress(String address) {
        //getActiveCustomer().setAddress(address);
        newBusinessCustomer.setAddress(address);
        newPrivateCustomer.setAddress(address);
    }
    
    public void setLastName(String lastName){
        newPrivateCustomer.setLastName(lastName);
        }
    public String getLastName(){
        return newPrivateCustomer.getLastName();
    }
    
    public void setFirstName(String firstName) {
        newPrivateCustomer.setFirstName(firstName);
    }

    public String getFirstName() {
        return newPrivateCustomer.getFirstName();
    }
    
    public void setPesel(String pesel) {
        newPrivateCustomer.setPesel(pesel);
    }

    public String getPesel() {
        return newPrivateCustomer.getPesel();
    }

    
}
